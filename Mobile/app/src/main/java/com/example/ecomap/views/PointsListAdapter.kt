package com.example.ecomap.views

import android.content.res.ColorStateList
import android.graphics.Color
import android.text.BoringLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.ecomap.R
import com.example.ecomap.dto.ObjectTypeDTO
import com.example.ecomap.dto.RecyclingPointInfo

class PointsListViewHolder(
    itemview : View,
    onClick: (position : Int) -> Unit,
    onLongClick: (position : Int) -> Boolean) : RecyclerView.ViewHolder(itemview)
{
    var layout : LinearLayout
    var name : TextView
    var adress : TextView

    init {
        layout = itemview.findViewById(R.id.pointsBottomList)
        layout.setOnClickListener{
            onClick(layoutPosition)
        }
        layout.setOnLongClickListener {
            onLongClick(layoutPosition)
        }
        name = itemview.findViewById(R.id.pointName)
        name.setOnClickListener{
            onClick(layoutPosition)
        }
        name.setOnLongClickListener {
            onLongClick(layoutPosition)
        }
        adress = itemview.findViewById(R.id.pointAdress)
        adress.setOnClickListener{
            onClick(layoutPosition)
        }
        adress.setOnLongClickListener {
            onLongClick(layoutPosition)
        }
    }
}

class PointsListAdapter(
    private val items: List<RecyclingPointInfo>,
    private val onClick: (position : Int) -> Unit,
    private val onLongClick: (position : Int) -> Boolean) : RecyclerView.Adapter<PointsListViewHolder>()
{
    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder : PointsListViewHolder, position : Int) {
        holder.name.text = items[position].Name
        holder.adress.text = items[position].Adress
    }

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getItemViewType(position : Int) = position + 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PointsListViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.points_list, parent, false);
        return PointsListViewHolder(itemView, onClick, onLongClick)
    }
}