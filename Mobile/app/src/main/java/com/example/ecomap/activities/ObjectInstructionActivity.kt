package com.example.ecomap.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.example.ecomap.R
import com.example.ecomap.dto.ObjectRecognitionResult
import com.example.ecomap.dto.ObjectTypeDTO
import com.example.ecomap.views.ObjectInstructionAdapter
import com.google.gson.Gson

class ObjectInstructionActivity : AppCompatActivity() {

    private lateinit var data: List<ObjectTypeDTO>

    private fun initView(){
        val toMap = findViewById<TextView>(R.id.toMap)
        toMap.setOnClickListener{
            intent.putExtra("data", Gson().toJson(data.map { it.Id }))
            val intent = Intent(applicationContext, MapActivity::class.java)
            startActivity(intent)
        }
        val instructionViewPager = findViewById<ViewPager>(R.id.instructionViewPager)
        instructionViewPager.adapter = ObjectInstructionAdapter(data)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_object_instruction)

        val extras = intent.extras
        if (extras != null) {
            val jsonData = extras.getString("data")
            data = Gson().fromJson(jsonData, Array<ObjectTypeDTO>::class.java).asList()
        }

        initView()
    }
}