package com.example.ecomap.dto

class RecyclingPointInfo(
    val Id : Int,
    val Name : String,
    val Description : String?,
    val Adress : String,
    val Longitude : Float,
    val Latitude : Float,
    val Distance : Double,
    val AcceptingObjects : String,
    val Phone : String?
)