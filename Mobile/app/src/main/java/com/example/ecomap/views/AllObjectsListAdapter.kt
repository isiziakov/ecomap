package com.example.ecomap.views

import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.ecomap.R
import com.example.ecomap.dto.ObjectTypeDTO

class AllObjectsListAdapter(
    private val items: List<ObjectTypeDTO>,
    private val selected : Int = -1,
    private val onClick: (position : Int, selected : Int) -> Unit,) : RecyclerView.Adapter<ObjectsListViewHolder>()
{
    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder : ObjectsListViewHolder, position : Int) {
        holder.name.text = items[position].Name
        if (selected == items[position].Id){
            holder.layout.backgroundTintList = ColorStateList.valueOf(Color.parseColor("#34FA3C"))
        }
    }

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getItemViewType(position : Int) = position + 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ObjectsListViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.objects_list_adapter, parent, false);
        return ObjectsListViewHolder(itemView) {
            position -> onClick(position, selected)
        }
    }
}