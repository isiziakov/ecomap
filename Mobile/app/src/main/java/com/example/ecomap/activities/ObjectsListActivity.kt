package com.example.ecomap.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.example.ecomap.R
import com.example.ecomap.dto.ObjectRecognitionResult
import com.example.ecomap.dto.ObjectTypeDTO
import com.example.ecomap.views.AllObjectsListAdapter
import com.example.ecomap.views.ObjectsListAdapter
import com.example.ecomap.views.ObjectsListViewHolder
import com.example.ecomap.web.WebApi
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import com.google.gson.Gson


class ObjectsListActivity : AppCompatActivity() {

    private lateinit var recycleView : RecyclerView
    private lateinit var adapter: Adapter<ObjectsListViewHolder>
    private lateinit var data: ObjectRecognitionResult
    private lateinit var bottomSheetBehavior : BottomSheetBehavior<LinearLayout>
    private lateinit var bottomSheetRV : RecyclerView
    private val currentObjects: MutableList<ObjectTypeDTO> = mutableListOf()
    private val allFreeObjects: MutableList<ObjectTypeDTO> = mutableListOf()

    private fun initView() {
        val back = findViewById<ImageButton>(R.id.objectsListBack)
        val toInstruction = findViewById<ImageButton>(R.id.objectsListToInstruction)
        back?.setOnClickListener {
            finish()
        }
        toInstruction.setOnClickListener {
            if (currentObjects.size == 0){
                Toast.makeText(applicationContext, "Не выбран ни один объект", Toast.LENGTH_SHORT).show()
            }
            else{
                WebApi.getObjectInstruction(currentObjects.map { i -> i.Id }) {
                        result ->
                    if (result != null) {
                        val intent = Intent(applicationContext, ObjectInstructionActivity::class.java)
                        intent.putExtra("data", result)
                        startActivity(intent)
                    }
                    else{
                        val toast = Toast.makeText(applicationContext, "Произошла ошибка при отправке изображения, проверьте соединение с интернетом", Toast.LENGTH_SHORT)
                        toast.show()
                    }
                }
            }
        }
        recycleView = findViewById(R.id.objectsListRV)
        val mLayoutManager = LinearLayoutManager(this)
        recycleView.layoutManager = mLayoutManager
        adapter = ObjectsListAdapter(currentObjects) { position -> onObjectsListClick(position) }
        recycleView.adapter = adapter

        val simpleItemTouchCallback = ItemTouchHelper(
            object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT or ItemTouchHelper.LEFT) {
                override fun onMove(
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder,
                    target: RecyclerView.ViewHolder
                ): Boolean {
                    return true
                }

                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                    val pos = viewHolder.layoutPosition
                    currentObjects.removeAt(pos)
                    adapter.notifyItemRemoved(pos)
                    adapter.notifyItemChanged(pos)
                }

                override fun getSwipeDirs (recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
                    if (viewHolder.layoutPosition == currentObjects.size) return 0
                    return super.getSwipeDirs(recyclerView, viewHolder)
                }
            })
        simpleItemTouchCallback.attachToRecyclerView(recycleView)

        bottomSheetRV = findViewById(R.id.listRV)
        bottomSheetRV.layoutManager = LinearLayoutManager(bottomSheetRV.context)

        val listObjectsBottomSheet = findViewById<LinearLayout>(R.id.list_bottom_sheet)
        bottomSheetBehavior = BottomSheetBehavior.from(listObjectsBottomSheet)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        bottomSheetBehavior.addBottomSheetCallback(object : BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED)
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            }
            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val extras = intent.extras
        if (extras != null) {
            val jsonData = extras.getString("data")
            data = Gson().fromJson(jsonData, ObjectRecognitionResult::class.java)
            for (i in data.AllTypes){
                if (data.RecognisedTypes.find { it == i.Id } != null){
                    currentObjects.add(i)
                }
            }
        }

        setContentView(R.layout.activity_objects_list)

        initView()
    }

    private fun onObjectsListClick(position: Int){
        allFreeObjects.clear()
        var selected = -1
        if (position != currentObjects.size){
            allFreeObjects.add(currentObjects[position])
            selected = currentObjects[position].Id
        }
        for (i in data.AllTypes){
            if (currentObjects.find { it.Id == i.Id } == null){
                allFreeObjects.add(i)
            }
        }
        val adapter = AllObjectsListAdapter(allFreeObjects, selected) {
                pos, sel -> onAllObjectsListClick(pos, sel)
        }
        bottomSheetRV.adapter = adapter
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    private fun onAllObjectsListClick(position: Int, selected: Int){
        if (selected == -1){
            currentObjects.add(allFreeObjects[position])
            adapter.notifyItemInserted(currentObjects.size - 1)
        }
        else {
            val pos = currentObjects.indexOfFirst { i -> i.Id == selected }
            if (pos != -1){
                currentObjects[pos] = allFreeObjects[position]
                adapter.notifyItemChanged(pos)
            }
        }
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
    }
}