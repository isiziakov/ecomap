package com.example.ecomap.activities

import android.Manifest
import android.content.Context
import android.location.LocationManager
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ecomap.R
import com.example.ecomap.dto.ObjectTypeDTO
import com.example.ecomap.dto.ObjectsForFinding
import com.example.ecomap.dto.RecyclingPointInfo
import com.example.ecomap.views.PointsListAdapter
import com.example.ecomap.web.WebApi
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.gson.Gson
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.ItemizedIconOverlay
import org.osmdroid.views.overlay.ItemizedOverlayWithFocus
import org.osmdroid.views.overlay.OverlayItem
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay


class MapActivity : AppCompatActivity() {
    private lateinit var map: MapView
    private lateinit var points : List<RecyclingPointInfo>
    private lateinit var bottomSheetBehavior : BottomSheetBehavior<LinearLayout>
    private lateinit var bottomSheetRV : RecyclerView
    private lateinit var items : ArrayList<OverlayItem>
    private var selectedItemPos : Int = -1
    private lateinit var ids : List<Int>

    private fun initView(){
        map = findViewById(R.id.map)
        map.setTileSource(TileSourceFactory.MAPNIK)
        map.setBuiltInZoomControls(false)
        map.setMultiTouchControls(true)
        map.controller.setZoom(17.0)
        map.isTilesScaledToDpi = true

        setUserPosition()
    }

    private fun setUserPosition(){
        registerForActivityResult(ActivityResultContracts.RequestPermission()){}.launch(Manifest.permission.ACCESS_FINE_LOCATION)

        val provider = GpsMyLocationProvider(this)
        provider.addLocationSource(LocationManager.NETWORK_PROVIDER)
        val mMyLocationOverlay = MyLocationNewOverlay(provider, map)
        mMyLocationOverlay.enableMyLocation()
        mMyLocationOverlay.isDrawAccuracyEnabled = false
        mMyLocationOverlay.runOnFirstFix{
            runOnUiThread{
                map.controller.setCenter(mMyLocationOverlay.myLocation)
            }
            val objects = ObjectsForFinding(ids, mMyLocationOverlay.myLocation.longitude.toFloat(), mMyLocationOverlay.myLocation.latitude.toFloat())
            WebApi.getObjectRecyclingPoints(objects){
                result ->
                if (result != null){
                    points = Gson().fromJson(result, Array<RecyclingPointInfo>::class.java).asList()
                    setPoints()
                    runOnUiThread {
                        setBottomSheet()
                    }
                }
            }
        }
        map.overlays.add(mMyLocationOverlay)
    }

    private fun setBottomSheet(){
        bottomSheetRV = findViewById(R.id.listRV)
        bottomSheetRV.layoutManager = LinearLayoutManager(bottomSheetRV.context)
        bottomSheetRV.adapter = PointsListAdapter(points,
            onClick = { pos -> onBottomSheetClick(pos) },
            onLongClick = { pos -> onBottomSheetLongClick(pos) })

        val listObjectsBottomSheet = findViewById<LinearLayout>(R.id.list_bottom_sheet)
        bottomSheetBehavior = BottomSheetBehavior.from(listObjectsBottomSheet)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        bottomSheetBehavior.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN)
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }
            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
        })
    }

    private fun setPoints(){
        items = ArrayList()
        for (p in points){
            items.add(OverlayItem("", "",
                GeoPoint(p.Latitude.toDouble(), p.Longitude.toDouble())))
            items.last().setMarker(ContextCompat.getDrawable(this, R.drawable.marker_red))
        }

        val mOverlay = ItemizedOverlayWithFocus(this, items,
        object : ItemizedIconOverlay.OnItemGestureListener<OverlayItem> {
            override fun onItemSingleTapUp(index: Int, item: OverlayItem?): Boolean {
                return onItemSingleTapUpSelect(index, item)
            }

            override fun onItemLongPress(index: Int, item: OverlayItem?): Boolean {
                showFullPointInfo(index)
                return true
            }

        })
        mOverlay.setFocusItemsOnTap(false)

        map.overlays.add(mOverlay)
    }

    private fun showFullPointInfo(position: Int){
        showFullPointInfo(points[position])
    }

    private fun showFullPointInfo(item: RecyclingPointInfo){
        val builder: AlertDialog.Builder = AlertDialog.Builder(this).setPositiveButton(
            "Закрыть") {
                dialog, id ->  dialog.cancel()}
        val frameView = FrameLayout(this)
        builder.setView(frameView)

        val alertDialog: AlertDialog = builder.create()
        val inflater: LayoutInflater = alertDialog.layoutInflater
        val view = inflater.inflate(R.layout.point_full_info, frameView)
        val layout = view.findViewById<LinearLayout>(R.id.fullPointInfo)
        val name = layout.findViewById<TextView>(R.id.fullPointInfoName)
        name.text = item.Name
        val adress = layout.findViewById<TextView>(R.id.fullPointInfoAdress)
        adress.text = item.Adress
        val description = layout.findViewById<TextView>(R.id.fullPointInfoDescription)
        if (item.Description == null || item.Description == ""){
            description.visibility = View.GONE
        }
        else{
            description.text = item.Description
        }
        val phone = layout.findViewById<TextView>(R.id.fullPointInfoContacts)
        if (item.Phone == null || item.Phone == ""){
            phone.visibility = View.GONE
        }
        else{
            phone.text = item.Phone
        }
        val accepting = layout.findViewById<TextView>(R.id.fullPointInfoAccepting)
        accepting.text = item.AcceptingObjects

        alertDialog.show()
    }

    private fun onBottomSheetClick(position : Int){
        if (position != selectedItemPos){
            items[position].setMarker(ContextCompat.getDrawable(this, R.drawable.marker_selected))
            if (selectedItemPos > -1){
                items[selectedItemPos].setMarker(ContextCompat.getDrawable(this, R.drawable.marker_red))
            }
            selectedItemPos = position
        }
        runOnUiThread{
            map.controller.animateTo(GeoPoint(
                points[position].Latitude.toDouble(),
                points[position].Longitude.toDouble()
            ))
        }
    }

    private fun onBottomSheetLongClick(position : Int) : Boolean{
        showFullPointInfo(position)
        return true
    }

    private fun onItemSingleTapUpSelect(index: Int, item: OverlayItem?): Boolean{
        if (index != selectedItemPos){
            items[index].setMarker(ContextCompat.getDrawable(this, R.drawable.marker_selected))
            if (selectedItemPos > -1){
                items[selectedItemPos].setMarker(ContextCompat.getDrawable(this, R.drawable.marker_red))
            }
            selectedItemPos = index
            bottomSheetRV.layoutManager?.scrollToPosition(index)
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val ctx: Context = this
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx))

        setContentView(R.layout.activity_map)

        val extras = intent.extras
        if (extras != null) {
            val jsonData = extras.getString("data")
            ids = Gson().fromJson(jsonData, Array<Int>::class.java).asList()
        }

        initView()
    }

    override fun onResume() {
        super.onResume()
        map.onResume()
    }

    override fun onPause() {
        super.onPause()
        map.onPause()
    }
}