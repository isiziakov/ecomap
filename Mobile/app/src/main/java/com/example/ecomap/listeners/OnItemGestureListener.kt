package com.example.ecomap.listeners

import org.osmdroid.views.overlay.ItemizedIconOverlay
import org.osmdroid.views.overlay.OverlayItem

class OnItemGestureListener() : ItemizedIconOverlay.OnItemGestureListener<OverlayItem> {
    override fun onItemSingleTapUp(index: Int, item: OverlayItem?): Boolean {
        return true
    }

    override fun onItemLongPress(index: Int, item: OverlayItem?): Boolean {
        return true
    }
}