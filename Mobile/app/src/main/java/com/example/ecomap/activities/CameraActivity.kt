package com.example.ecomap.activities

import android.Manifest
import android.content.ContentValues.TAG
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.ecomap.R
import com.example.ecomap.helpful.ImageHelper
import com.example.ecomap.web.WebApi
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class CameraActivity : AppCompatActivity() {
    private var imageCapture = ImageCapture.Builder().build()
    private lateinit var cameraExecutor: ExecutorService
    private lateinit var cameraView: PreviewView
    private var type : String? = ""

    private fun initView(){
        findViewById<Button>(R.id.cameraButton)?.setOnClickListener {
            takePhoto()
        }
        cameraView = findViewById(R.id.cameraView)
    }

    private fun takePhoto() {
        imageCapture.takePicture(
            ContextCompat.getMainExecutor(this),
            object : ImageCapture.OnImageCapturedCallback() {
                override fun onCaptureSuccess(image: ImageProxy) {
                    val bitmap = ImageHelper.imageProxyToBitmap(image)
                    val base64 = ImageHelper.encodeToBase64(bitmap)
                    if (base64 != null){
                        WebApi.recognizeImageObject(base64) {
                            result ->
                            if (result != null) {
                                if (type == "Object"){
                                    val intent = Intent(applicationContext, ObjectsListActivity::class.java)
                                    intent.putExtra("data", result)
                                    startActivity(intent)
                                    finish()
                                }
                                if (type == "Code"){
                                    TODO()
                                }
                            }
                            else{
                                val toast = Toast.makeText(applicationContext, "Произошла ошибка при отправке изображения, проверьте соединение с интернетом", Toast.LENGTH_SHORT)
                                toast.show()
                            }
                        }
                    }
                }
            }
        )
    }

    private fun startCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(this)

        cameraProviderFuture.addListener({
            // Used to bind the lifecycle of cameras to the lifecycle owner
            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()

            // Preview
            val preview = Preview.Builder()
                .build()
                .also {
                    it.setSurfaceProvider(cameraView.surfaceProvider)
                }

            // Select back camera as a default
            val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA

            try {
                // Unbind use cases before rebinding
                cameraProvider.unbindAll()

                // Bind use cases to camera
                cameraProvider.bindToLifecycle(
                    this, cameraSelector, imageCapture, preview)

            } catch(exc: Exception) {
                Log.e(TAG, "Use case binding failed", exc)
            }

        }, ContextCompat.getMainExecutor(this))
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), 400)
    }

    private fun allPermissionsGranted() = ContextCompat.checkSelfPermission(this,
        Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_camera)

        initView()

        val extras = intent.extras
        if (extras != null) {
            type = extras.getString("type")
        }

        if (allPermissionsGranted()) {
            startCamera()
        } else {
            requestPermissions()
        }

        cameraExecutor = Executors.newSingleThreadExecutor()
    }

    override fun onDestroy() {
        super.onDestroy()
        cameraExecutor.shutdown()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        for (p in grantResults){
            if (p != PackageManager.PERMISSION_GRANTED){
                val toast = Toast.makeText(applicationContext, "Необходимо предоставить доступ к камере", Toast.LENGTH_SHORT)
                toast.show()
                finish()
            }
        }
        startCamera()
    }
}