package com.example.ecomap.dto

class ObjectTypeDTO(
    val Id : Int,
    val Name : String,
    val Class : String,
    val Instruction : String,
    val ImageData: ByteArray)