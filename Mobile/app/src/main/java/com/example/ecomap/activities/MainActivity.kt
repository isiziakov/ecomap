package com.example.ecomap.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import com.example.ecomap.R

class MainActivity : AppCompatActivity() {

    private fun initView() {
        val mainCode = findViewById<Button>(R.id.mainCode)
        val mainObject = findViewById<Button>(R.id.mainObject)
        val mainData = findViewById<Button>(R.id.mainData)
        val mainUrl = findViewById<Button>(R.id.mainUrl)
        mainCode?.setOnClickListener {
            showWillBeAddedToast()
        }
        mainObject.setOnClickListener {
            val intent = Intent(this, CameraActivity::class.java)
            intent.putExtra("type", "Object")
            startActivity(intent)
        }
        mainData.setOnClickListener {
            showWillBeAddedToast()
        }
        mainUrl.setOnClickListener {
            showWillBeAddedToast()
        }
    }

    private fun showWillBeAddedToast(){
        val text = "Будет добавлено в следующих версиях"
        val duration = Toast.LENGTH_SHORT

        val toast = Toast.makeText(applicationContext, text, duration)
        toast.show()
    }

    private fun setupHelpfulObjects(){
        PreferencesHelper.setup(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initView()

        setupHelpfulObjects()
    }
}