package com.example.ecomap.dto

class ObjectRecognitionResult(
    val AllTypes: List<ObjectTypeDTO>,
    val RecognisedTypes : List<Int>
)