package com.example.ecomap.dto

class ObjectsForFinding(
    val ids : List<Int>,
    val longitude : Float,
    val latitude : Float
)