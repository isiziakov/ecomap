package com.example.ecomap.activities

import android.content.Context
import android.content.SharedPreferences

object PreferencesHelper {
    private lateinit var prefs: SharedPreferences
    private lateinit var editor : SharedPreferences.Editor

    fun setup(context : Context){
        prefs = context.getSharedPreferences("mobile", Context.MODE_PRIVATE)
        editor = prefs.edit()
    }

    fun getString(key : String, default : String) = prefs.getString(key, default)

    fun putString(key : String, value : String){
        editor.putString(key, value)
        editor.apply()
    }
}