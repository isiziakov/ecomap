package com.example.ecomap.web

import com.example.ecomap.dto.ObjectRecognitionResult
import com.example.ecomap.dto.ObjectsForFinding
import com.google.gson.Gson
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.IOException


object WebApi {
    private val client = OkHttpClient()
    private val gson = Gson()
    private const val baseUrl = "http://10.0.2.2:5000/"
    private val JSON = "application/json; charset=utf-8".toMediaType()

    private fun createGet(url: String) = Request.Builder()
        .url(baseUrl + url)
        .build()

    private fun createPost(url: String, json: String) = Request.Builder()
        .url(baseUrl + url)
        .post(json.toRequestBody(JSON))
        .build()

    private fun createPut(url: String, json: String) = Request.Builder()
        .url(baseUrl + url)
        .put(json.toRequestBody(JSON))
        .build()

    private fun createDelete(url: String, json: String) = Request.Builder()
        .url(baseUrl + url)
        .delete(json.toRequestBody(JSON))
        .build()

    fun recognizeImageObject(image : String, result : (result: String?) -> Unit){
        val request = createPost("RecognitionObject",
            gson.toJson(image).toString())
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                result(null)
            }

            override fun onResponse(call: Call, response: Response) {
                result(response.body?.string())
            }
        })
    }

    fun getObjectInstruction(objects: List<Int>, result : (result: String?) -> Unit){
        val request = createPost("Objects",
            gson.toJson(objects).toString())
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                result(null)
            }

            override fun onResponse(call: Call, response: Response) {
                result(response.body?.string())
            }
        })
    }

    fun getObjectRecyclingPoints(objects : ObjectsForFinding, result : (result: String?) -> Unit){
        val request = createPost("ObjectsMap",
            gson.toJson(objects).toString())
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                result(null)
            }

            override fun onResponse(call: Call, response: Response) {
                result(response.body?.string())
            }
        })
    }
}