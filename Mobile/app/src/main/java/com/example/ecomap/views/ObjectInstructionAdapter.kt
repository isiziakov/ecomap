package com.example.ecomap.views

import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.example.ecomap.R
import com.example.ecomap.dto.ObjectTypeDTO

class ObjectInstructionAdapter(private val items : List<ObjectTypeDTO>) : PagerAdapter() {
    override fun getCount() = items.size

    override fun isViewFromObject(view: View, `object`: Any) = view == `object`

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(container.context).inflate(R.layout.instruction_item, container, false)
        val name = view.findViewById<TextView>(R.id.instructionName)
        val data = view.findViewById<TextView>(R.id.instructionInstruction)
        val image = view.findViewById<ImageView>(R.id.instructionImage)
        name.text = items[position].Name
        data.text = items[position].Instruction
        image.setImageBitmap(BitmapFactory.decodeByteArray(items[position].ImageData,
            0, items[position].ImageData.size))
        (container as ViewPager).addView(view)
        return view
    }
}