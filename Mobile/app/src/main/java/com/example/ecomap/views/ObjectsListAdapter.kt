package com.example.ecomap.views

import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.ecomap.R
import com.example.ecomap.dto.ObjectTypeDTO

class ObjectsListViewHolder(itemview : View, onClick: (position : Int) -> Unit) : RecyclerView.ViewHolder(itemview)
{
    var layout : LinearLayout
    var name : TextView

    init {
        layout = itemview.findViewById(R.id.objectsListAdapterView)
        layout.setOnClickListener{
            onClick(layoutPosition)
        }
        name = itemview.findViewById(R.id.objectsListItemName)
        name.setOnClickListener{
            onClick(layoutPosition)
        }
    }
}

class ObjectsListAdapter(private val items: List<ObjectTypeDTO>, private val onClick: (position : Int) -> Unit) : RecyclerView.Adapter<ObjectsListViewHolder>()
{
    override fun getItemCount() = items.size + 1

    override fun onBindViewHolder(holder : ObjectsListViewHolder, position : Int) {
        if (position < items.size) {
            holder.name.isClickable = false
            holder.name.setTextColor(Color.parseColor("#000000"))
            holder.layout.backgroundTintList = ColorStateList.valueOf(Color.parseColor("#D9D9D9"))
            holder.name.text = items[position].Name
            holder.name.textSize = 30f
        }
        else{
            holder.name.isClickable = true
            holder.name.setTextColor(Color.parseColor("#0027F4"))
            holder.layout.backgroundTintList = ColorStateList.valueOf(Color.WHITE)
            holder.name.text = "Добавить"
            holder.name.textSize = 24f
        }
    }

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getItemViewType(position : Int) = position + 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ObjectsListViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.objects_list_adapter, parent, false);
        return ObjectsListViewHolder(itemView, onClick)
    }
}