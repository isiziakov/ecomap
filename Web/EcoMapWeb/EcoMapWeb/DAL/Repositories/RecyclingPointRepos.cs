﻿using EcoMapWeb.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace EcoMapWeb.DAL.Repositories
{
    public class RecyclingPointRepos : IRepository<RecyclingPoint>
    {
        private EcoMapContext db;

        public RecyclingPointRepos(EcoMapContext dbcontext)
        {
            this.db = dbcontext;
        }

        public List<RecyclingPoint> GetList()
        {
            return db.RecyclingPoints.ToList();
        }

        public RecyclingPoint? GetItem(int id)
        {
            return db.RecyclingPoints.Where(i => i.Id == id).SingleOrDefault();
        }

        public void Create(RecyclingPoint item)
        {
            db.RecyclingPoints.Add(item);
        }

        public void Update(RecyclingPoint item)
        {
            db.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            RecyclingPoint item = db.RecyclingPoints.Find(id);
            if (item != null)
                db.RecyclingPoints.Remove(item);
        }

        public int Save()
        {
            return db.SaveChanges();
        }
    }
}
