﻿using EcoMapWeb.DAL.Interfaces;

namespace EcoMapWeb.DAL.Repositories
{
    //Scaffold-DbContext "Server=DESKTOP-TSE3JH5\SQLEXPRESS;Database=Mobile;Trusted_Connection=True;" Microsoft.EntityFrameworkCore.SqlServer
    public class DbRepos : IDbRepos
    {
        private EcoMapContext db;
        private ObjectTypeRepos objectType;
        private StreetRepos street;
        private TownRepos town;
        private ObjectPointRepos objectPoint;
        private RecyclingPointRepos recyclingPoint;

        public DbRepos()
        {
            db = new EcoMapContext();
        }

        public IRepository<ObjectType> ObjectType
        {
            get
            {
                if (objectType == null)
                    objectType = new ObjectTypeRepos(db);
                return objectType;
            }
        }

        public IRepository<Street> Street
        {
            get
            {
                if (street == null)
                    street = new StreetRepos(db);
                return street;
            }
        }

        public IRepository<Town> Town
        {
            get
            {
                if (town == null)
                    town = new TownRepos(db);
                return town;
            }
        }

        public IRepository<ObjectPoint> ObjectPoint
        {
            get
            {
                if (objectPoint == null)
                    objectPoint = new ObjectPointRepos(db);
                return objectPoint;
            }
        }

        public IRepository<RecyclingPoint> RecyclingPoint
        {
            get
            {
                if (recyclingPoint == null)
                    recyclingPoint = new RecyclingPointRepos(db);
                return recyclingPoint;
            }
        }
    }
}
