﻿using EcoMapWeb.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace EcoMapWeb.DAL.Repositories
{
    public class ObjectTypeRepos : IRepository<ObjectType>
    {
        private EcoMapContext db;

        public ObjectTypeRepos(EcoMapContext dbcontext)
        {
            this.db = dbcontext;
        }

        public List<ObjectType> GetList()
        {
            return db.ObjectTypes.ToList();
        }

        public ObjectType? GetItem(int id)
        {
            return db.ObjectTypes.Where(i => i.Id == id).SingleOrDefault();
        }

        public void Create(ObjectType item)
        {
            db.ObjectTypes.Add(item);
        }

        public void Update(ObjectType item)
        {
            db.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            ObjectType item = db.ObjectTypes.Find(id);
            if (item != null)
                db.ObjectTypes.Remove(item);
        }

        public int Save()
        {
            return db.SaveChanges();
        }
    }
}
