﻿using EcoMapWeb.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace EcoMapWeb.DAL.Repositories
{
    public class TownRepos : IRepository<Town>
    {
        private EcoMapContext db;

        public TownRepos(EcoMapContext dbcontext)
        {
            this.db = dbcontext;
        }

        public List<Town> GetList()
        {
            return db.Towns.ToList();
        }

        public Town? GetItem(int id)
        {
            return db.Towns.Where(i => i.Id == id).SingleOrDefault();
        }

        public void Create(Town item)
        {
            db.Towns.Add(item);
        }

        public void Update(Town item)
        {
            db.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Town item = db.Towns.Find(id);
            if (item != null)
                db.Towns.Remove(item);
        }

        public int Save()
        {
            return db.SaveChanges();
        }
    }
}
