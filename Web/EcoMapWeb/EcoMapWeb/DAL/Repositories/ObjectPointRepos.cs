﻿using EcoMapWeb.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace EcoMapWeb.DAL.Repositories
{
    public class ObjectPointRepos : IRepository<ObjectPoint>
    {
        private EcoMapContext db;

        public ObjectPointRepos(EcoMapContext dbcontext)
        {
            this.db = dbcontext;
        }

        public List<ObjectPoint> GetList()
        {
            return db.ObjectPoints.ToList();
        }

        public ObjectPoint? GetItem(int id)
        {
            return db.ObjectPoints.Where(i => i.Id == id).SingleOrDefault();
        }

        public void Create(ObjectPoint item)
        {
            db.ObjectPoints.Add(item);
        }

        public void Update(ObjectPoint item)
        {
            db.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            ObjectPoint item = db.ObjectPoints.Find(id);
            if (item != null)
                db.ObjectPoints.Remove(item);
        }

        public int Save()
        {
            return db.SaveChanges();
        }
    }
}
