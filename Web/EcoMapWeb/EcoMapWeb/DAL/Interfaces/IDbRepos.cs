﻿namespace EcoMapWeb.DAL.Interfaces
{
    public interface IDbRepos // интерфейс для взаимодействия с репозиториями
    {
        IRepository<ObjectType> ObjectType { get; }
        IRepository<Town> Town { get; }
        IRepository<Street> Street { get; }
        IRepository<RecyclingPoint> RecyclingPoint { get; }
        IRepository<ObjectPoint> ObjectPoint { get; }

    }
}
