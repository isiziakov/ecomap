﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EcoMapWeb
{
    public partial class ObjectPoint
    {
        public int Id { get; set; }
        public int PointId { get; set; }
        public int ObjectTypeId { get; set; }
    }
}
