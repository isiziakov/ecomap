﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EcoMapWeb
{
    public partial class RecyclingPoint
    {
        public int Id { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public int StreetId { get; set; }
        public string Description { get; set; }
        public string Phone { get; set; }
        public string Name { get; set; }
        public string Adress { get; set; }
    }
}
