﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EcoMapWeb
{
    public partial class ObjectType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Instruction { get; set; }
        public string Class { get; set; }
    }
}
