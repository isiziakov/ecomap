﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EcoMapWeb
{
    public partial class Street
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TownId { get; set; }
    }
}
