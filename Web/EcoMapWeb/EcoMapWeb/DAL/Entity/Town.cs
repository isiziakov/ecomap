﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EcoMapWeb
{
    public partial class Town
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
