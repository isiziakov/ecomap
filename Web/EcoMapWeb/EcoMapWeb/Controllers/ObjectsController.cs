﻿using EcoMapWeb.DTO.DB;
using EcoMapWeb.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace EcoMapWeb.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ObjectsController : Controller
    {
        [HttpPost]
        public IActionResult GetObjects([FromBody] List<int> ids)
        {
            List<ObjectTypeDTO> objects = new List<ObjectTypeDTO>();
            using (var db = new DbOperations())
            {
                foreach (int id in ids)
                {
                    objects.Add(db.GetObjectType(id));
                    objects.Last().SetImage();
                }
            }
            return Ok(JsonConvert.SerializeObject(objects));
        }
    }
}