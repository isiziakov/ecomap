﻿using EcoMapWeb.DTO;
using EcoMapWeb.DTO.DB;
using EcoMapWeb.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace EcoMapWeb.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ObjectsMapController : Controller
    {
        [HttpPost]
        public IActionResult GetObjects([FromBody] ObjectsForFinding request)
        {
            List<RecyclingPointDTO> points = new List<RecyclingPointDTO>();
            var mapPoints = new List<RecyclingPointInfo>();
            using (var db = new DbOperations())
            {
                points = db.GetRecyclingPoints().Where(i => i.AcceptingObjects.Select(p => (int)p.Id).ToList().Intersect(request.ids).Count() == request.ids.Count()).ToList();
                foreach (var p in points)
                {
                    mapPoints.Add(new RecyclingPointInfo(p));
                    mapPoints.Last().SetDistance(request.latitude, request.longitude);
                }
                mapPoints = mapPoints.Where(i => i.Distance <= 50.0).OrderBy(i => i.Distance).ToList();
            }
            return Ok(JsonConvert.SerializeObject(mapPoints));
        }
    }
}
