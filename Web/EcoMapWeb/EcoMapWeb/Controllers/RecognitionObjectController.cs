﻿using EcoMapWeb.DTO;
using EcoMapWeb.Operations;
using Microsoft.AspNetCore.Mvc;

namespace EcoMapWeb.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RecognitionObjectController : Controller
    {
        [HttpPost]
        public IActionResult Recognise([FromBody] string data)
        {
            ObjectRecognitionResult result;
            using (var op = new RecognitionOperations())
            {
                result = op.Recognise(data, "Object");
            }
            return Ok(result.ToJson());
        }
    }
}
