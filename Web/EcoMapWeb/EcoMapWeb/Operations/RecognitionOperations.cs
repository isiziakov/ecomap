﻿using EcoMapWeb.DTO;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;

namespace EcoMapWeb.Operations
{
    public class RecognitionOperations : IDisposable
    {
        private string id;
        private string path;
        private string resultPath;

        public RecognitionOperations()
        {
            id = Guid.NewGuid().ToString();
            path = Path.Combine(Directory.GetCurrentDirectory(), id + ".jpg");
            resultPath = Path.Combine(Directory.GetCurrentDirectory(), id);
        }

        public ObjectRecognitionResult Recognise(string image, string type)
        {
            SaveImage(image);
            switch (type)
            {
                case "Object":
                    return RecogniseObject();
                case "Code":
                    return RecogniseCode();
            }
            return null;
        }

        public ObjectRecognitionResult RecogniseObject()
        {
            var pyPath = "C:/Users/ivans/PycharmProjects/yolo";
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = "/C " + pyPath + "/venv/Scripts/python.exe " + pyPath + "/yolov5/detect.py" + " --img 1024 --weights " + pyPath + "/yolov5/runs/train/result18/weights/best.pt --source " + path + " --save-txt --conf-thres 0.3 --exist-ok --name " + resultPath;
            process.StartInfo = startInfo;
            process.Start();
            process.WaitForExit();
            return new ObjectRecognitionResult(GetObjectResult());
        }

        public List<int> GetObjectResult()
        {
            var db = new DbOperations();
            var result = new List<int>();
            var data = File.ReadAllLines(resultPath + "/labels/" + id + ".txt");
            foreach (var line in data)
            {
                var numbers = line.Split(' ');
                if (db.GetObjectType(int.Parse(numbers[0])) != null)
                {
                    if (result.FindIndex(i => i == int.Parse(numbers[0])) == -1){
                        result.Add(int.Parse(numbers[0]));
                    }
                }
            }
            return result;
        }

        public ObjectRecognitionResult RecogniseCode()
        {
            return null;
        }

        private void SaveImage(string image)
        {
            File.WriteAllBytes(path, Convert.FromBase64String(image));
        }

        public void Dispose()
        {
            if (File.Exists(path))
            {
                File.Delete(path);
                Directory.Delete(resultPath, true);
            }
        }

        public void FindCodes()
        {

        }
    }
}
