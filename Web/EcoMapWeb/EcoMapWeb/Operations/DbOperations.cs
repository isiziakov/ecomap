﻿using EcoMapWeb.DAL.Repositories;
using EcoMapWeb.DTO.DB;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EcoMapWeb.Operations
{
    public class DbOperations : IDisposable
    {
        DbRepos db;
        public DbOperations()
        {
            db = new DbRepos();
        }

        public List<ObjectTypeDTO> GetObjectTypes()
        {
            return db.ObjectType.GetList().Select(i => new ObjectTypeDTO(i)).ToList();
        }

        public ObjectTypeDTO? GetObjectType(int id)
        {
            ObjectType? o = db.ObjectType.GetItem(id);
            return o != null ? new ObjectTypeDTO(o) : null;
        }

        public int AddObjectType(ObjectTypeDTO objectType)
        {
            ObjectType newObjectType = new ObjectType();
            newObjectType.Instruction = objectType.Instruction; 
            newObjectType.Name = objectType.Name;
            newObjectType.Class = objectType.Class;
            newObjectType.Image = objectType.Image;
            db.ObjectType.Create(newObjectType);
            db.ObjectType.Save();
            return newObjectType.Id;
        }

        public void UpdateObjectType(ObjectTypeDTO objectType)
        {
            ObjectType newObjectType = new ObjectType();
            newObjectType.Id = objectType.Id;
            newObjectType.Class = objectType.Class;
            newObjectType.Image = objectType.Image;
            newObjectType.Instruction = objectType.Instruction;
            newObjectType.Name = objectType.Name;
            db.ObjectType.Update(newObjectType);
            db.ObjectType.Save();
        }

        public void DeleteObjectType(int id)
        {
            db.ObjectType.Delete(id);
        }

        public void Dispose()
        {
            db = null;
        }

        public StreetDTO? GetStreet(int id)
        {
            Street? o = db.Street.GetItem(id);
            return o != null ? new StreetDTO(o) : null;
        }

        public List<ObjectPointDTO> GetObjectPoints()
        {
            return db.ObjectPoint.GetList().Select(i => new ObjectPointDTO(i)).ToList();
        }

        public List<ObjectTypeDTO> GetObjectTypeForPoint(int id)
        {
            var result = new List<ObjectTypeDTO>();
            var points = GetObjectPoints().Where(i => i.PointId == id).ToList();
            foreach(var type in points)
            {
                result.Add(GetObjectType(type.ObjectTypeId));
            }
            return result;
        }

        public List<RecyclingPointDTO> GetRecyclingPoints()
        {
            return db.RecyclingPoint.GetList().Select(i => new RecyclingPointDTO(i)).ToList();
        }
    }
}
