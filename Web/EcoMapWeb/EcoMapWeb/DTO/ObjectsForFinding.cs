﻿using System.Collections.Generic;

namespace EcoMapWeb.DTO
{
    public class ObjectsForFinding
    {
        public List<int> ids { get; set; }
        public float longitude { get; set; }
        public float latitude { get; set; }

        public ObjectsForFinding()
        {

        }
    }
}
