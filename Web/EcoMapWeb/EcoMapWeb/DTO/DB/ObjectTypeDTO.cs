﻿using System.IO;
using System.Linq;

namespace EcoMapWeb.DTO.DB
{
    public class ObjectTypeDTO
    {
        public int Id;
        public string Name;
        public string Image;
        public int[] ImageData;
        public string Instruction;
        public string Class;

        public ObjectTypeDTO()
        {

        }

        public ObjectTypeDTO(ObjectType objectType)
        {
            Id = objectType.Id;
            Name = objectType.Name;
            Image = objectType.Image;
            Instruction = objectType.Instruction;
            Class = objectType.Class;
        }

        public void SetImage()
        {
            ImageData = File.ReadAllBytes(Image).Select(i => (int)i).ToArray();
        }
    }
}
