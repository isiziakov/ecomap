﻿using EcoMapWeb.Operations;
using System.Collections.Generic;

namespace EcoMapWeb.DTO.DB
{
    public class RecyclingPointDTO
    {
        public int Id;
        public float Latitude;
        public float Longitude;
        public StreetDTO Street;
        public string Description;
        public string Phone;
        public string Name;
        public string Adress;
        public List<ObjectTypeDTO> AcceptingObjects;

        public RecyclingPointDTO()
        {

        }

        public RecyclingPointDTO(RecyclingPoint recyclingPoint)
        {
            Id = recyclingPoint.Id;
            Latitude = recyclingPoint.Latitude;
            Longitude = recyclingPoint.Longitude;
            Description = recyclingPoint.Description;
            Phone = recyclingPoint.Phone;
            Name = recyclingPoint.Name;
            Adress = recyclingPoint.Adress;
            using (DbOperations db = new DbOperations())
            {
                Street = db.GetStreet(recyclingPoint.StreetId);
                AcceptingObjects = db.GetObjectTypeForPoint(Id);
            }
        }
    }
}
