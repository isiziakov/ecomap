﻿namespace EcoMapWeb.DTO.DB
{
    public class ObjectPointDTO
    {
        public int Id;
        public int PointId;
        public int ObjectTypeId;

        public ObjectPointDTO()
        {

        }

        public ObjectPointDTO(ObjectPoint objectPoint)
        {
            Id = objectPoint.Id;
            PointId = objectPoint.PointId;
            ObjectTypeId = objectPoint.ObjectTypeId;
        }
    }
}
