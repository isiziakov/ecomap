﻿namespace EcoMapWeb.DTO.DB
{
    public class StreetDTO
    {
        public int Id;
        public string Name;
        public int TownId;

        public StreetDTO()
        {

        }

        public StreetDTO(Street street)
        {
            Id = street.Id;
            Name = street.Name;
            TownId = street.TownId;
        }
    }
}
