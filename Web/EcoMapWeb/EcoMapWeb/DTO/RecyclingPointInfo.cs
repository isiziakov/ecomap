﻿using EcoMapWeb.DTO.DB;
using System;
using System.Device.Location;

namespace EcoMapWeb.DTO
{
    public class RecyclingPointInfo
    {
        public int Id;
        public string Name;
        public string Description;
        public string Adress;
        public float Longitude;
        public float Latitude;
        public double Distance;
        public string AcceptingObjects = "";
        public string Phone;

        public RecyclingPointInfo()
        {

        }

        public RecyclingPointInfo(RecyclingPointDTO recyclingPoint)
        {
            Id = recyclingPoint.Id;
            Name = recyclingPoint.Name;
            Description = recyclingPoint.Description;
            Phone = recyclingPoint.Phone;
            Latitude = recyclingPoint.Latitude;
            Longitude = recyclingPoint.Longitude;
            Adress = recyclingPoint.Street.Name + ", " + recyclingPoint.Adress;
            for(int i = 0; i < recyclingPoint.AcceptingObjects.Count; i++)
            {
                if (i > 0)
                {
                    AcceptingObjects += recyclingPoint.AcceptingObjects[i].Name.ToLower();
                }
                else
                {
                    AcceptingObjects += recyclingPoint.AcceptingObjects[i].Name;
                }
                if (i != recyclingPoint.AcceptingObjects.Count - 1)
                {
                    AcceptingObjects += ", ";
                }
            }
        }

        public void SetDistance(float latitude, float longitude)
        {
            GeoCoordinate c1 = new GeoCoordinate(latitude, longitude);
            GeoCoordinate c2 = new GeoCoordinate(Latitude, Longitude);

            Distance = c1.GetDistanceTo(c2) / 1000;
        }
    }
}
