﻿using EcoMapWeb.DTO.DB;
using EcoMapWeb.Operations;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace EcoMapWeb.DTO
{
    public class ObjectRecognitionResult
    {
        public List<ObjectTypeDTO> AllTypes;
        public List<int> RecognisedTypes;

        public ObjectRecognitionResult()
        {
            CreateAllTypesList();
        }

        public ObjectRecognitionResult(List<int> recognisedTypes)
        {
            CreateAllTypesList();
            RecognisedTypes = recognisedTypes;
        }

        private void CreateAllTypesList()
        {
            using (var db = new DbOperations())
            {
                AllTypes = db.GetObjectTypes();
            }
            foreach (var o in AllTypes)
            {
                o.Instruction = "";
                o.Image = "";
            }
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
